#!/usr/bin/python
# -*- coding: utf-8 -*-

import json
import requests
import xml.etree.ElementTree as ET
from bs4 import BeautifulSoup


class XMLTool():

    def __init__(self, xml_url=None, xml_file=None, xml_string=None):

        if xml_url:
            # Get a XML from a URL source
            self.xml = self.__set_from_url(xml_url)
        elif xml_file:
            # Get a XML from a file
            self.xml = self.__set_from_file(xml_file)
        elif xml_string:
            # Get a XML from a string
            self.xml = self.__set_from_string(xml_string)
        else:
            raise ValueError('You need to pass a XML source')

    def xml_to_json(self, filename=False):
        '''
        Convert a XML to JSON.
        If a filename is passed as argument, write a json file.
        '''

        # Defining json output format and json filename
        json_obj = {"feed": []}

        root = self.xml
        for elt in root.iter():
            if elt.tag == "item":
                item_obj = self.__item_iterator(elt)
                json_obj["feed"].append(item_obj)

        if filename:
            self.__write_json_file(json_obj, filename)
            return

        return json_obj

    def __set_from_url(self, url):
        '''
        Get a XML from a URL source.

        Keyword arguments:
        url -- URL source
        '''
        r = requests.get(url).text.encode("utf-8")
        return self.__set_from_string(r)

    def __set_from_file(self, filename):

        return ET.parse(filename).getroot()

    def __set_from_string(self, string):

        return ET.ElementTree(ET.fromstring(string))

    def __item_iterator(self, item):
        '''
        Iterate over an item and return a json object.

        Keyword arguments:
        item -- A XML item :
                Ex: <item>
                        <title>Item example</title>
                        <link> https://example.com </link>
                        <description>
                            [<div class="example"> <p> Test </p> </div>]
                        </description>
                    </item>
        '''

        json_item = {}

        for elt in item.iter():

            elt_obj = None

            if elt.tag == "title" or elt.tag == "link":
                elt_obj = elt.text

            elif elt.tag == "description":
                elt_obj = self.__description_iterator(elt.text)

            if elt_obj is not None:
                json_item[elt.tag] = elt_obj

        return json_item

    def __description_iterator(self, description):
        '''
        Iterate over a description item and return a list of json objects.

        Keyword arguments:
        description -- A description item :
                Ex:
                    <description>
                        [<div class="example"> <p> Test </p> </div>]
                    </description>
        '''

        description_list = []

        html_tree = BeautifulSoup(description, "lxml")
        for elt in html_tree.body.contents:

            descr_obj = None

            if elt.name == "p":
                if elt.text.strip():
                    descr_obj = {"type": "text", "content": elt.text.strip()}

            elif elt.name == "div":

                elt_children = elt.children

                child = next(elt_children, False)
                while child:

                    if child.name == "img":
                        descr_obj = {"type": "image", "content": child["src"]}
                        break

                    if child.name == "ul":
                        links = child.find_all("a")
                        for index, link in enumerate(links):
                            links[index] = link["href"]
                            descr_obj = {"type": "links", "content": links}
                        break

                    child = next(elt_children, False)

            if descr_obj is not None:
                description_list.append(descr_obj)

        return description_list

    def __write_json_file(self, json_obj, filename):
        '''
        Write a .json file.

        Keyword arguments:
        json_obj - json dictionary
        filename - json filename
        '''

        final_json = json.dumps(json_obj, sort_keys=True,
                                indent=3, separators=(',', ': '),
                                ensure_ascii=False).encode('utf8')

        with open(filename, "a+") as json_file:
            json_file.write(final_json)

        return
