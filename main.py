#!/usr/bin/python
# -*- coding: utf-8 -*-

from core.lib import XMLTool


def main():

    # Defining XML source
    URL = "http://revistaautoesporte.globo.com/rss/ultimas/feed.xml"

    # Creating XMLTool object (passing the URL)
    xml_obj = XMLTool(xml_url=URL)

    # Converting XML to JSON and saving in a .json file
    xml_obj.xml_to_json(xml_url="./feed.json")


if __name__ == "__main__":
    main()
