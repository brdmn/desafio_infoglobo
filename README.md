# Infoglobo's Challenge

This is a feed crawler for : http://revistaautoesporte.globo.com/rss/ultimas/feed.xml

You can convert this XML feed to a JSON file like: 

```xml
    {
        'feed': [
            {
                'title': 'titulo da materia',
                'link': 'url da materia',
                'description': [
                    {
                        'type': 'text',
                        'content': 'conteudo da tag'
                    },
                    {
                        'type': 'image',
                        'content': 'url da imagem'
                    },
                    {
                        'type': 'links',
                        'content': ['urls dos links', ...]
                    }
                ]
            }
        ]
    }
```
### Prerequisites :

	1. Unix Shell
	2. Python >= 2.7 
	3. BeautifulSoup package
	
    To install packages you can run:
    1. pip install -r requirements.txt


### Running:

	You can run this code doing:
	    1. cd desafio_infoglobo
		2. python main.py

### Tests:
    
    You can run tests doing:
        1. cd desafio_infoglobo
		2. python -m unittest discover